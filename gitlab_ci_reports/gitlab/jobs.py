from . import api_request

def list_pipeline_jobs(project_id, pipeline_id):
    url = f"/projects/{project_id}/pipelines/{pipeline_id}/jobs"
    params = dict(
      per_page = 100
    )
    return api_request.request_gitlab_api(url, params)

def list_bridge_jobs(project_id, pipeline_id):
    url = f"/projects/{project_id}/pipelines/{pipeline_id}/bridges"
    params = dict(
      per_page = 100
    )
    return api_request.request_gitlab_api(url, params)    
