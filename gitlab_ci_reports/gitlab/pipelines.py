from . import api_request

def retrive_pipelines(project_id, updated_after):
    url = f"/projects/{project_id}/pipelines"
    params = dict(
      updated_after = updated_after,
      per_page = 100
    )
    return api_request.request_gitlab_api(url, params)

def retrive_pipeline_variables(project_id, pipeline_id):
    url = f"/projects/{project_id}/pipelines/{pipeline_id}/variables"
    params = dict(
      per_page = 100
    )
    return api_request.request_gitlab_api(url, params)
