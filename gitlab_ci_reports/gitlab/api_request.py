import requests
import os

API_TOKEN = os.environ['GITLAB_API_TOKEN']
API_SERVER = "https://gitlab.com/api/v4/"

def request_gitlab_api(url, params):
    url=f"{API_SERVER}/{url}"
    pipeline_headers = {'PRIVATE-TOKEN': API_TOKEN}
    pipeline_jobs = requests.get(url=url, headers=pipeline_headers, params=params)
    return pipeline_jobs.json()