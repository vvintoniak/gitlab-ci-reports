import os
import datetime
import json
from gitlab import jobs, pipelines

PROJECT_ID = "22086708"

def calculate_date_after(days):
    date_N_days_ago = datetime.datetime.now() - datetime.timedelta(days=days)
    return date_N_days_ago
    
def main():
    updated_after = calculate_date_after(5)
    pipeline_results = pipelines.retrive_pipelines(PROJECT_ID, updated_after)
    for pipeline in pipeline_results:
        pipeline_id = pipeline["id"]
        print(pipelines.retrive_pipeline_variables(PROJECT_ID, pipeline_id))
        print(jobs.list_bridge_jobs(PROJECT_ID, pipeline_id))

if __name__ == "__main__":
    main()